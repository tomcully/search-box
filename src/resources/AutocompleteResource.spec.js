import AutocompleteResource from './AutocompleteResource';
import Resource from './Resource';

describe('given the autocomplete resource', () => {
  let getSpy;
  beforeEach(() => {
    getSpy = jest.spyOn(Resource, 'get');
  });
  afterEach(() => {
    getSpy.mockReset();
  });

  function thenResourceIsNotCalled() {
    expect(getSpy).not.toHaveBeenCalled();
  }

  function thenGetIsCalledWithEncoded(searchTerm) {
    expect(getSpy).toHaveBeenCalledWith(`FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=${encodeURIComponent(searchTerm)}`);
  }

  describe('when given empty search term', () => {
    let result;
    beforeEach(() => {
      result = AutocompleteResource.searchAhead('');
    });

    it('then an empty array is returned', () => {
      expect(result).toEqual([]);
    });

    it('then resource is not called', () => {
      thenResourceIsNotCalled();
    });
  });
  describe('when search term is undefined', () => {
    let result;
    beforeEach(() => {
      result = AutocompleteResource.searchAhead();
    });

    it('then an empty array is returned', () => {
      expect(result).toEqual([]);
    });

    it('then resource is not called', () => {
      thenResourceIsNotCalled();
    });
  });

  describe('when no results are found', () => {
    const NO_RESULTS_SEARCH_TERM = 'term with no results';
    let result;
    beforeEach(async () => {
      getSpy.mockImplementation(() => Promise.resolve({
        data: {
          results: {
            numFound: 0,
          },
        },
      }));
      result = await AutocompleteResource.searchAhead(NO_RESULTS_SEARCH_TERM);
    });
    it('then get is called with expected and encoded search term', () => {
      thenGetIsCalledWithEncoded(NO_RESULTS_SEARCH_TERM);
    });
    it('then an empty array is returned', () => {
      expect(result).toEqual([]);
    });
  });

  describe('when results are returned', () => {
    const SUCCESSFUL_SEARCH_TERM = 'some successful search term';
    const RESULTS = {
      numFound: 8429,
      docs: [
        'one suggestion',
        'another suggestion',
        'one more suggestion',
      ],
    };
    let result;

    beforeEach(async () => {
      getSpy.mockImplementation(() => Promise.resolve({
        data: {
          results: RESULTS,
        },
      }));
      result = await AutocompleteResource.searchAhead(SUCCESSFUL_SEARCH_TERM);
    });

    it('then get is called with expected and encoded search term', () => {
      thenGetIsCalledWithEncoded(SUCCESSFUL_SEARCH_TERM);
    });

    it('then the docs from response are returned', () => {
      expect(result).toEqual(RESULTS.docs);
    });
  });
});
