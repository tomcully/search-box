import Resource from './Resource';

class AutocompleteResource {
  searchAhead = (searchTerm) => {
    if (!searchTerm || searchTerm.length < 1) {
      return [];
    }
    const searchPath = `FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=${encodeURIComponent(searchTerm)}`;
    return Resource.get(searchPath)
      .then((result) => {
        if (!result || !result.data || !result.data.results || result.data.results.numFound < 1) {
          return [];
        }
        return result.data.results.docs;
      });
  }
}

export default new AutocompleteResource();
