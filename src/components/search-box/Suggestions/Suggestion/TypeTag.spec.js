import { mount } from '@vue/test-utils';
import TypeTag from './TypeTag.vue';

describe('given a type tag component', () => {
  describe('when placeType is A', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'A',
        },
      });
    });
    it('then airport tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--airport').exists()).toBe(true);
    });
  });
  describe('when placeType is C', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'C',
        },
      });
    });
    it('then city tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--city').exists()).toBe(true);
    });
  });
  describe('when placeType is T', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'T',
        },
      });
    });
    it('then station tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--station').exists()).toBe(true);
    });
  });
  describe('when placeType is Z', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'Z',
        },
      });
    });
    it('then place tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--place').exists()).toBe(true);
    });
  });
  describe('when placeType is D', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'D',
        },
      });
    });
    it('then district tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--district').exists()).toBe(true);
    });
  });
  describe('when placeType is W', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'W',
        },
      });
    });
    it('then country tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--country').exists()).toBe(true);
    });
  });
  describe('when placeType is F', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount(TypeTag, {
        propsData: {
          typeTag: 'F',
        },
      });
    });
    it('then region tag is displayed', () => {
      expect(wrapper.find('.type-tag__pill--region').exists()).toBe(true);
    });
  });
});
