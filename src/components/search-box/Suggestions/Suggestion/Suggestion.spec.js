import { shallowMount } from '@vue/test-utils';
import Suggestion from './Suggestion.vue';
import TypeTag from './TypeTag.vue';

describe('given a suggestion component', () => {
  let wrapper;
  const RESULT = {
    city: 'a city',
    country: 'some-country',
    iata: 'iata',
    name: 'name',
    placeType: 'P',
    region: 'Region',
  };
  beforeEach(() => {
    wrapper = shallowMount(Suggestion, {
      propsData: {
        result: RESULT,
      },
    });
  });

  it('then placeType is passed to type tag', () => {
    expect(wrapper.find(TypeTag).attributes('type-tag')).toBe(RESULT.placeType);
  });

  it('then name is displayed correctly', () => {
    expect(wrapper.find('.suggestion__name').text()).toBe(`${RESULT.name} (${RESULT.iata})`);
  });

  it('then name subtext is displayed correctly', () => {
    expect(wrapper.find('.suggestion__name-subtext').text()).toBe(`${RESULT.region}, ${RESULT.city}, ${RESULT.country}`);
  });

  describe('when result has no iata', () => {
    const { iata, ...NO_IATA_RESULT } = RESULT;
    beforeEach(() => {
      wrapper.setProps({
        result: NO_IATA_RESULT,
      });
      wrapper.vm.$nextTick();
    });
    it('then name is displayed correctly', () => {
      expect(wrapper.find('.suggestion__name').text()).toBe(RESULT.name);
    });
  });

  describe('when the result has no region', () => {
    const { region, ...NO_REGION_RESULT } = RESULT;
    beforeEach(() => {
      wrapper.setProps({
        result: NO_REGION_RESULT,
      });
      wrapper.vm.$nextTick();
    });
    it('then name subtext is displayed correctly', () => {
      expect(wrapper.find('.suggestion__name-subtext').text()).toBe(`${RESULT.city}, ${RESULT.country}`);
    });
  });

  describe('when the result has no city', () => {
    const { city, ...NO_CITY_RESULT } = RESULT;
    beforeEach(() => {
      wrapper.setProps({
        result: NO_CITY_RESULT,
      });
      wrapper.vm.$nextTick();
    });
    it('then name subtext is displayed correctly', () => {
      expect(wrapper.find('.suggestion__name-subtext').text()).toBe(`${RESULT.region}, ${RESULT.country}`);
    });
  });

  describe('when the result has no country', () => {
    const { country, ...NO_COUNTRY_RESULT } = RESULT;
    beforeEach(() => {
      wrapper.setProps({
        result: NO_COUNTRY_RESULT,
      });
      wrapper.vm.$nextTick();
    });
    it('then name subtext is displayed correctly', () => {
      expect(wrapper.find('.suggestion__name-subtext').text()).toBe(`${RESULT.region}, ${RESULT.city}`);
    });
  });
});
