import { shallowMount } from '@vue/test-utils';
import Suggestions from './Suggestions.vue';
import Suggestion from './Suggestion/Suggestion.vue';

describe('given the suggestions component', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Suggestions, {
      propsData: {
        results: [],
      },
    });
  });

  function whenResultsPropIs(results) {
    wrapper.setProps({
      results,
    });
  }

  describe('when results is empty', () => {
    it('then No results found message is displayed', () => {
      expect(wrapper.find('.suggestions__no-results').isVisible()).toBe(true);
      expect(wrapper.find('.suggestions__no-results').text()).toBe('No results found');
    });
    it('then no suggestion component is added', () => {
      expect(wrapper.find(Suggestion).exists()).toBe(false);
    });
  });

  describe('when results is not empty', () => {
    const EXPECTED_RESULTS = [
      {
        suggestion: 1,
      },
      {
        suggestion: 2,
      },
      {
        suggestion: 3,
      },
    ];
    beforeEach(() => {
      whenResultsPropIs(EXPECTED_RESULTS);
    });
    it('then each result is displayed as a suggestion', () => {
      expect(wrapper.findAll(Suggestion).at(0).vm.result).toEqual(EXPECTED_RESULTS[0]);
      expect(wrapper.findAll(Suggestion).at(1).vm.result).toEqual(EXPECTED_RESULTS[1]);
      expect(wrapper.findAll(Suggestion).at(2).vm.result).toEqual(EXPECTED_RESULTS[2]);
    });
  });
});
