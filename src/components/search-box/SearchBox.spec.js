import { mount } from '@vue/test-utils';
import SearchBox from '.';
import AutocompleteResource from '../../resources/AutocompleteResource';
import Suggestions from './Suggestions/Suggestions.vue';

jest.useFakeTimers();

describe('given a search box', () => {
  let wrapper;
  let searchBoxInput;
  let searchAheadSpy;
  beforeEach(() => {
    wrapper = mount(SearchBox);
    searchBoxInput = wrapper.find('#search-box__input');
    searchAheadSpy = jest.spyOn(AutocompleteResource, 'searchAhead');
    searchAheadSpy.mockImplementation(() => {});
  });
  afterEach(() => {
    searchAheadSpy.mockReset();
  });

  function whenTheUserHasTyped(typedChars) {
    wrapper.vm.searchBoxValue = typedChars;
    searchBoxInput.trigger('keyup');
    jest.runAllTimers();
  }

  function thenResultsPassedToSuggestionsAre(expectedResults) {
    expect(wrapper.find(Suggestions).vm.results).toBe(expectedResults);
  }

  it('then the title has the correct copy', () => {
    expect(wrapper.find('h2').text()).toBe('Where are you going?');
  });
  it('then the search input has the correct label', () => {
    expect(wrapper.find('label').attributes('for')).toBe('search-box__input');
    expect(wrapper.find('label').text()).toBe('Pick-up Location');
  });
  it('then the input has the correct placeholder', () => {
    expect(searchBoxInput.attributes('placeholder')).toBe('city, airport, station, region, district...');
  });

  describe('when the user types in 1 letter', () => {
    beforeEach(() => {
      whenTheUserHasTyped('t');
    });
    it('then autocomplete resource is not called', () => {
      expect(searchAheadSpy).not.toHaveBeenCalled();
    });
    it('then search suggestions do not exist', () => {
      expect(wrapper.find(Suggestions).exists()).toBe(false);
    });
  });

  describe('when the user types a second letter', () => {
    const EXPECTED_SEARCH_TERM = 'te';
    const RESULTS = [
      {
        some: 'data',
      }, {
        some: 'more-data',
      },
    ];

    beforeEach(() => {
      searchAheadSpy.mockImplementation(() => Promise.resolve(RESULTS));
      whenTheUserHasTyped(EXPECTED_SEARCH_TERM);
    });

    it('then search ahead is called with the typed string', () => {
      expect(searchAheadSpy).toHaveBeenCalledWith(EXPECTED_SEARCH_TERM);
    });
    it('then the results are assigned to the search suggstions', () => {
      thenResultsPassedToSuggestionsAre(RESULTS);
    });
    it('then search results exist', () => {
      expect(wrapper.find(Suggestions).exists()).toBe(true);
    });

    describe('when user types changes the string', () => {
      const NEW_RESULTS = [];
      const NEW_SEARCH_TERM = 'test';
      beforeEach(() => {
        searchAheadSpy.mockImplementation(() => Promise.resolve(NEW_RESULTS));
        whenTheUserHasTyped(NEW_SEARCH_TERM);
      });
      it('then search ahead is called with new string', () => {
        expect(searchAheadSpy).toHaveBeenCalledWith(NEW_SEARCH_TERM);
      });
      it('then the results passed to suggestions is updated', () => {
        thenResultsPassedToSuggestionsAre(NEW_RESULTS);
      });
    });

    describe('when the user shortens the string to be shorter than 2 characters', () => {
      const SHORTER_SEARCH_TERM = 't';
      beforeEach(() => {
        whenTheUserHasTyped(SHORTER_SEARCH_TERM);
      });
      it('then search ahead is not called again', () => {
        expect(searchAheadSpy).not.toHaveBeenCalledWith(SHORTER_SEARCH_TERM);
      });
      it('then results are set to empty array', () => {
        expect(wrapper.vm.results).toEqual([]);
      });
    });
  });
});
